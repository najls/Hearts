class Filler(object):
    def fill(self, matrix, start_pos):
        # for adding expanded positions
        stack = []
        
        if not self.valid_pos(start_pos, len(matrix)):
            start_pos = (0, 0)
            
        # p is the current position
        p = start_pos
        stack.append(p)
        while len(stack) != 0:
            p = stack.pop()
            matrix[p[1]][p[0]] = 1
            neighbors = self.get_neighbors(matrix, p)
            # add neighbors to stack
            for neighbor in neighbors:
                #if neighbor not in visited:
                stack.append(neighbor)
                
    # p is current position
    def get_neighbors(self, matrix, p):
        s = len(matrix)
        ret = []
        
        # check and add upper, right,lower and left neighbor
        if self.valid_pos((p[0], p[1] - 1), s) and matrix[p[1] - 1][p[0]] != 1:
            ret.append((p[0], p[1] - 1))
        if self.valid_pos((p[0] + 1, p[1]), s) and matrix[p[1]][p[0] + 1] != 1:
            ret.append((p[0] + 1, p[1]))
        if self.valid_pos((p[0], p[1] + 1), s) and matrix[p[1] + 1][p[0]] != 1:
            ret.append((p[0], p[1] + 1))
        if self.valid_pos((p[0] - 1, p[1]), s) and matrix[p[1]][p[0] - 1] != 1:
            ret.append((p[0] - 1, p[1]))
            
        return ret
        
    def valid_pos(self, pos, max):
        return 0 <= pos[0] and pos[0] < max and 0 <= pos[1] and pos[1] < max
