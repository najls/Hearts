#!/usr/bin/env python

import sys
import math
from time import sleep

from Filler import Filler

def circle(origin, radius, segment = 360, offset = 0):
    """ Return a representation of a circle or circular segment in the form of a
    list of tuples, where each tuple represents a coordinate. The function
    "draws" clockwise and places the 0 degrees point at the bottom of the circle
    or circular segment.
    
    Keyword arguments:
    origin  -- a tuple
    radius  -- a float
    segment -- an int representing the length of the segment in degrees
    offset  -- and in representing the offset of the segments starting point in
               degrees
    """
    ret = []
    if offset < 0 or 360 < offset:
        offset = 0
    if segment < 0 or 360 < segment:
        segment = 0
        
    """ the larger the circle, the larger 'points' has to be, or
    the circle starts breaking up, but 360 covers the needs for this program and
    is convenient for when using segment and offset """
    points = 360
    i = points - offset
    while i > points - offset - segment:
        x = int(origin[0] + radius * math.sin(2 * math.pi * i / points))
        y = int(origin[1] + radius * math.cos(2 * math.pi * i / points))
        ret.append((x, y))
        i -= 1
    return ret
    
def trace(matrix, *curves):
    size = len(matrix)
    for curve in curves:
        for point in curve:
            if point[1] < size and point[0] < size:
                matrix[point[1]][point[0]] = 1
                
def fill(matrix, start_pos):
    # creates a Filler object, which works kind of like the bucket in MS Paint
    filler = Filler()
    filler.fill(matrix, start_pos)
    
def print_matrix(matrix, blank, fill):
    for y in range(len(matrix)):
        for x in range(len(matrix[y])):
            if matrix[y][x] == 0:
                sys.stdout.write(blank)
                everyother_0 = True
            elif matrix[y][x] == 1:
                sys.stdout.write(fill)
                everyother_1 = True
        sys.stdout.flush()
        sleep(0.02)
        sys.stdout.write('\n')
        
def main(argv):
    try:
        size = int(argv[1])
    except (ValueError, IndexError):
        size = 33
    else:
        if size < 0 or 99 < size:
            size = 99
            
    # list containing circular segments that make up the shape
    c = []
    
    if size is not 0:
        matrix = [[0 for x in range(size)] for y in range(size)]
        # these are the circular segments
        c.append(circle((size * 0.3, size * 0.3), size * 0.25, 145, 90))
        c.append(circle((size * 0.7, size * 0.3), size * 0.25, 145, 126))
        c.append(circle((size * 0.62, size * 0.38), size * 0.58, 89, 11))
        c.append(circle((size * 0.38, size * 0.38), size * 0.58, 89, 261))
    else:
        matrix = [[0 for x in range(20)] for y in range(20)]
        c.append(circle((10, 10), 7.5))
        
    # draw shape in matrix
    trace(matrix, *c)
    
    if size is not 0:
        # fill the shape
        fill(matrix, (size // 2, size // 2))
        print_matrix(matrix, "  ", "<3")
    else:
        print_matrix(matrix, " ", "0")
        
if __name__ == "__main__":
    main(sys.argv)
